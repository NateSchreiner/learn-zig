const std = @import("std");
const Allocator = std.mem.Allocator;
const expect = std.testing.expect;

pub fn Node(comptime T: type) type {
    return struct {
        const Self = @This();
        key: T,
        value: T,

        next: ?*Node(T),
        prev: ?*Node(T),

        fn get(self: *Self, key: T) ?T {
            if (self.key == key) {
                return self.value;
            }

            if (self.next) |n| {
                return n.get(key);
            }

            return null;
        }

        fn insert(self: *Self, key: T, val: T, allocator: Allocator) !void {
            if (self.next) |n| {
                try n.insert(key, val, allocator);
            } else {
                var node = try allocator.create(Node(T));

                node.* = Node(T){ .key = key, .value = val, .next = null, .prev = self };
                self.next = node;
            }
        }

        fn sortedInsert(self: *Self, key: T, val: T, alloc: Allocator) !void {
            if (key < self.value) {
                var tmp = try alloc.create(Node(T));
                tmp.* = Node(T){
                    .key = key,
                    .value = val,
                    .next = self,
                    .prev = self.prev,
                };
                self.prev = tmp;
            } else if (self.next) |n| {
                try n.sortedInsert(key, val, alloc);
            }
        }

        fn remove(self: *Self, key: T, alloc: Allocator) ?*Node(T) {
            if (self.key == key) |n| {
                var tmp = self;
                n.prev.next = n.next;
                n.next.prev = n.prev;
                alloc.destroy(self);
                return tmp;
            } else {
                if (self.next) |n| {
                    return n.remove(key, alloc);
                }
            }
        }

        fn print(self: *Self) void {
            std.debug.print("{?}:{?} -> ", .{ self.key, self.value });
            if (self.next) |n| {
                n.print();
            }
        }
    };
}

pub fn SortedLinkedList(comptime T: type) type {
    return struct {
        const Self = @This();
        head: ?*Node(T),
        length: usize,
        allocator: Allocator,

        pub fn getHead(self: *Self) ?*Node(T) {
            if (self.head) |h| {
                return h;
            }

            return null;
        }

        pub fn get(self: Self, key: T) ?T {
            if (self.head) |h| {
                return h.get(key);
            }

            return null;
        }

        pub fn insert(self: *Self, key: T, val: T) !void {
            if (self.head) |n| {
                try n.insert(key, val, self.allocator);
            } else {
                var node = try self.allocator.create(Node(T));

                node.* = Node(T){
                    .key = key,
                    .value = val,
                    .next = null,
                    .prev = null,
                };
                self.head = node;
            }

            self.length += 1;
        }

        pub fn sortedInsert(self: *Self, key: T, val: T) !void {
            if (self.head) |n| {
                if (n.key < key) {
                    try n.sortedInsert(key, val, self.allocator);
                } else {
                    var tmp = try self.allocator.create(Node(T));
                    tmp.* = Node(T){
                        .key = key,
                        .value = val,
                        .next = n,
                        .prev = null,
                    };

                    if (n.next) |next| {
                        next.prev = tmp;
                    }

                    self.head = tmp;
                }

                self.length += 1;
            } else {
                var node = try self.allocator.create(Node(T));
                node.* = Node(T){
                    .key = key,
                    .value = val,
                    .next = null,
                    .prev = null,
                };
                self.head = node;
            }
        }

        pub fn printList(self: *Self) void {
            if (self.head) |n| {
                std.debug.print("{?}:{?} -> ", .{ n.key, n.value });
                if (n.next) |node| {
                    node.print();
                }
            }
        }

        pub fn remove(self: *Self, key: T) ?*Node(T) {
            if (self.head) |n| {
                if (n.key == key) {
                    var tmp = self.head;
                    self.head = n.next;
                    n.prev = null;
                    self.allocator.destroy(Node(T));
                    return tmp;
                } else if (n.next) |node| {
                    return node.next.remove(key, self.allocator);
                }
            }

            return null;
        }
    };
}

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();

    var intList = SortedLinkedList(i32){ .head = null, .length = 0, .allocator = allocator };
    try intList.sortedInsert(30, 20);
    try intList.sortedInsert(20, 10);
    try intList.sortedInsert(10, 100);

    const first = intList.get(20);
    try expect(first == 10);
    const second = intList.get(30);
    try expect(second == 20);
    const third = intList.get(10);
    try expect(third == 100);

    var val = intList.remove(20);

    if (val) |n| {
        try expect(n.key == 20);
        try expect(n.value == 10);
    }

    intList.printList();

    // intList.remove(20);

    // const head = intList.getHead();

    // std.debug.print("ListLength: {?}\n", .{intList.length});
    // if (head) |h| {
    //     std.debug.print("Val: {?}\n", .{h.value});
    // }
}
