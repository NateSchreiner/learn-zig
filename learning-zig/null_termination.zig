const std = @import("std");

pub fn main() void {
    const a = [3:false]bool{ false, true, false };
    var five = 5;
    std.debug.print("Five: {d}\n", .{five});
    std.debug.print("{any}\n", .{std.mem.asBytes(&a).*});

    const s = "test";
    const single = "n";
    std.debug.print("Byte: {any}\n", .{std.mem.asBytes(&single).*});
    std.debug.print("{any}\n", .{std.mem.asBytes(&s).*});
}
