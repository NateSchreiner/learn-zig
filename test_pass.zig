const std = @import("std");
const expect = std.testing.expect;

test "always succeeds" {
    try expect(true);
}

test "mutli-defer" {
    var x: f32 = 5;
    {
        defer x += 2;
        defer x /= 2;
    }
    try expect(x == 4.5);
}
