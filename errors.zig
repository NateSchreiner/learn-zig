const std = @import("std");

pub fn main() !void {
    tryError();
}

const FileOpenError = error{
    AccessDenied,
    OutOfMemory,
    FileNotFound,
};

fn tryError() void {
    var maybe: FileOpenError!u16 = 10;
    var no_error = maybe catch 0;
    std.debug.print("Error? {}\n", .{no_error});
    maybe = FileOpenError.AccessDenied;
    no_error = maybe catch 0;
    std.debug.print("Error? {}\n", .{no_error});
}
