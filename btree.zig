const std = @import("std");
const ArrayList = std.ArrayList;
const Queue = std.collections.Queue;
const allocator = std.heap.page_allocator;

pub fn BTree(comptime T: type) type {
    return struct {
        root: ?*Node(T),

        // Method to print the BTree in a basic, preorder traversal
        pub fn basicPrint(self: BTree(T)) void {
            if (self.root) |n| {
                n.preorderPrint();
            }

            std.debug.print("\n", .{});
        }

        // Doesn't seem to be a normal queue in std lib?
        // Use ArrayList and adjust with append() and orderedRemove(len(list)-1)
        pub fn breadthFirstPrint(self: BTree(T)) !void {
            var queue = ArrayList(?*Node(T)).init(allocator);
            defer queue.deinit();

            try queue.append(self.root);

            while (queue.items.len > 0) {
                var elem = queue.orderedRemove(queue.items.len - 1);
                if (elem) |n| {
                    try queue.append(n.left);
                    try queue.append(n.right);
                    std.debug.print("Val: {s}\n", .{n.value});
                }
            }
        }

        // pub fn fromListBreadth(self: *BTree(T), list: []?i32) !void {
        //     if (list.len == 0) {
        //         return;
        //     }

        //     var queue = Queue(*Node(T)).init();
        //     // var queue = Queue(*Node(T)).init(allocator);
        //     // defer queue.deinit();

        //     if (self.root == null) {
        //         self.root = try allocator.create(Node(T));
        //         self.root.?.value = list[0].?;
        //         // self.root.* = Node(T){ .value = list[0] };
        //     }

        //     try queue.put(self.root.?);

        //     var i: usize = 1;
        //     while (i < list.len) {
        //         const node = queue.get() orelse break;

        //         if (i < list.len) {
        //             node.left = Node(T){ .value = list[i] };
        //             try queue.put(node.left.?);
        //             i += 1;
        //         }

        //         if (i < list.len) {
        //             node.right = Node(T){ .value = list[i] };
        //             try queue.get(node.right.?);
        //             i += 1;
        //         }
        //     }
        // }

        // Issue with list type.
        // https://zig.news/gowind/beware-the-copy-32l4
        pub fn fromArrayListBreadth(self: *BTree(T), list: ArrayList(T)) !void {
            if (list.items.len == 0) {
                return;
            }

            var queue = Queue(*Node(T)).init(allocator);
            defer queue.deinit();

            self.root = Node(T){ .value = list.items[0] };
            try queue.enqueue(self.root.?);

            var i: usize = 1;
            while (i < list.items.len) {
                const node = queue.dequeue() orelse break;

                if (i < list.items.len) {
                    node.left = Node(T){ .value = list.items[i] };
                    try queue.enqueue(node.left.?);
                    i += 1;
                }

                if (i < list.items.len) {
                    node.right = Node(T){ .value = list.items[i] };
                    try queue.enqueue(node.right.?);
                    i += 1;
                }
            }
        }
    };
}

fn Node(comptime T: type) type {
    return struct {
        value: T,
        left: ?*Node(T),
        right: ?*Node(T),

        pub fn print(self: Node(T)) void {
            std.debug.print("NodeVal: {s}\n", .{self.value});
        }

        pub fn preorderPrint(self: Node(T)) void {
            std.debug.print("[ {s} ] ", .{self.value});
            if (self.left) |left| {
                left.preorderPrint();
            }

            if (self.right) |right| {
                right.preorderPrint();
            }
        }
    };
}

pub fn main() !void {
    // const StrNode = Node([]const u8);

    // const strValue = try allocator.dupe(u8, "THis is the root node!");
    // const two = try allocator.dupe(u8, "My Right nut!!");
    // const three = try allocator.dupe(u8, "This is my left nut!!! ");

    // var root = Node([]const u8){ .value = strValue, .left = null, .right = null };
    // var left = Node([]const u8){
    //     .value = two,
    //     .left = null,
    //     .right = null,
    // };

    // var right = Node([]const u8){
    //     .value = three,
    //     .left = null,
    //     .right = null,
    // };

    // root.left = &left;
    // root.right = &right;

    // const StrTree = BTree([]const u8);
    // const tree = StrTree{ .root = &root };
    // tree.basicPrint();
    // try tree.breadthFirstPrint();
    // comptime var list = ArrayList([]const u8).init(allocator);
    // try list.append("First");
    // try list.append("second");

    // _ = tree.fromListBreadth(list);

    // var intRoot = Node(i32){ .value = 8, .left = null, .right = null };

    // const IntTree = BTree(i32);
    // const intTree = IntTree{ .root = &intRoot };

    // var l: []?i32 = []?i32{};

    // var list: []?i32 = allocator.alloc(?i32, 0) catch {
    //     std.log.err("Allocation of list failed\n", .{});
    //     return;
    // };

    // list[0] = 8;
    // list[1] = 10;
    // list[2] = 20;
    // list[3] = 30;

    var intList = ArrayList(i32).init(allocator);
    try intList.append(8);
    try intList.append(10);
    try intList.append(20);
    try intList.append(30);

    // std.debug.print("IntList Length: {d}\n", .{list.len});

    // var tree = BTree(i32){ .root = null };
    // try tree.fromListBreadth(list);

    var treeTwo = BTree(i32){ .root = null };
    try treeTwo.fromArrayListBreadth(intList);

    // tree.basicPrint();
    treeTwo.basicPrint();
    // _ = IntTree.fromListBreadth(intList);
    // intTree.fromListBreadth(intList);
    // intRoot.print();
    // _ = intTree;

    // root.print();
}
