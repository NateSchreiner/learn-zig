const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "helloworld",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    exe.addIncludePath(.{ .path = "c-src" });

    // exe.addCSourceFile("source: CSourceFile")
    // const cSource = std.build.Step.Compile.CSourceFile{
    //     .file = std.build.LazyPath{
    //         .path = "c-src/hello.c",
    //     },
    //     .flags = &[_][]const u8{},
    //     // .flags = &[_][]const u8{"lstdc++"},
    // };
    // // exe.addIncludePath(std.build.LazyPath{
    // //     .path = "./",
    // // });
    // exe.addCSourceFile(cSource);
    // exe.linkLibCpp();

    // exe.install();

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the damn thing");
    run_step.dependOn(&run_cmd.step);
}
