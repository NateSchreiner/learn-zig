const std = @import("std");
const net = @import("std").net;

var n: [8]usize = undefined;

pub fn printNeighbor(id: usize) !void {
    n[id] = id + 100;
    std.time.sleep(1 * std.time.ns_per_s);
    var i = id;
    if (id == 0) {
        i = 8;
    }

    if (n.len >= i) {
        // std.debug.print("yielding...{d}\n", .{n.len});
        try std.Thread.yield();
        std.debug.print("Neighbor: {d} from {d}\n", .{ n[i - 1], i });
    } else {}
}

pub fn work(id: usize) void {
    std.time.sleep(1 * std.time.ns_per_s);
    std.debug.print("Id: {d}\n", .{id});
}

pub fn main() !void {
    const cpus = 8;
    var handles: [8]std.Thread = undefined;
    for (0..cpus) |i| {
        handles[i] = try std.Thread.spawn(.{}, printNeighbor, .{i});
    }

    for (handles) |h| h.join();
}
