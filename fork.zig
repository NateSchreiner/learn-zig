const std = @import("std");

// Would be nice to get a debugger for zig
// https://blog.tartanllama.xyz/writing-a-linux-debugger-setup/

pub fn main() !void {
    const pid = try std.os.fork();

    switch (pid) {
        0 => {
            std.debug.print("Child process\n", .{});
        },
        else => {
            std.debug.print("Parent process\n", .{});
        },
    }
}
