const std = @import("std");
const fs = std.fs;

pub fn main() !void {
    const bytes_written = try writeFile();
    std.debug.print("Bytes Written: {s}\n", .{bytes_written});
    const bytes_read = try readFile();
    std.debug.print("Bytes Read: {s}\n", .{bytes_read});

    if (bytes_read.len != bytes_written.len) {
        std.debug.print("Byte lengths differ, Written={d}, Read={d}\n", .{ bytes_written.len, bytes_read.len });
    }

    if (!std.mem.eql(u8, bytes_written, bytes_read)) {
        std.debug.print("Bytes  written and read don't match!!\n", .{});
    }
}

fn writeFile() ![]const u8 {
    const file = try fs.cwd().createFile(
        "test.txt",
        .{ .read = true },
    );

    defer file.close();

    const wrriten_string =
        \\ This is the first line
        \\ and we should probably try this
        \\ and also maybe we can read this one? 
        \\ how about a fourth one for the heckin frick?
    ;

    const bytes_written = try file.writeAll(wrriten_string);
    _ = bytes_written;

    return wrriten_string;
}

fn readFile() ![]const u8 {
    const file = try fs.cwd().openFile(
        "test.txt",
        .{},
    );

    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var streamer = buf_reader.reader();

    var buffer: [1024]u8 = undefined;

    var allocator = std.heap.page_allocator;

    var contents = std.ArrayList(u8).init(allocator);
    defer contents.deinit();

    while (try streamer.readUntilDelimiterOrEof(&buffer, '\n')) |line| {
        try contents.appendSlice(line);
        try contents.append('\n');
    }

    // return contents.toOwnedSlice().[0 .. contents.items.len - 2];
    _ = contents.pop();
    return contents.toOwnedSlice();
}
