const std = @import("std");

const User = struct {
    id: usize,
    power: u64,
};

pub fn main() void {
    var user = User{
        .id = 1,
        .power = 100,
    };

    levelUp(user);
    std.debug.print("Users power: {d}\n", .{user.power});
}

fn levelUp(user: User) void {
    std.debug.print("Leveling up!! {d}\n", .{user.power});
    // user.power += 1;
}
