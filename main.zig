const std = @import("std");

pub fn main() !void {
    std.debug.print("hello, {s}!\n", .{"world"});

    try userInput();
    // pointers();
    return;
    // const constant: i32 = 5;
    // var variable: i32 = 5000;

    // const array = [_]i32{ constant, variable, 1, 2, 4 };
    // std.debug.print("{any}\n", .{array});
    // for (array, 0..) |num, idx| {
    //     std.debug.print("Index: {} ", .{idx});
    //     std.debug.print("Num: {}\n", .{num});
    // }
}

fn userInput() !void {
    const stdin = std.io.getStdIn().reader();
    var data = [_]u8{0} * 10;

    if (try stdin.readUntilDelimiterOrEof(data[0..], '\n')) |user_input| {
        std.debug.print("{any}", .{user_input});
    } else {
        std.debug.print("error", .{});
    }

    // std.debug.print("Read: {}\n", .{sz});
    // std.debug.print("Length of Array: {}", .{data.len});
}

const MyStruct = struct { value: i32 };

fn pointers() void {
    var s = MyStruct{ .value = 32 };

    var val: i32 = 32;
    pprint(&val);
    structPrint(&s);
}

fn structPrint(s: *MyStruct) void {
    std.debug.print("StructVal: {}\n", .{s.value});
}

fn pprint(val: *i32) void {
    std.debug.print("Pointer: {}\n", .{val});
    std.debug.print("Value: {}\n", .{val.*});
}
