const std = @import("std");

const Element = struct {
    key: u32,
    value: []const u8,
    forward: []?*Element,

    const Self = @This();

    pub fn init(key: u32, val: []const u8, allocator: *const std.mem.Allocator, comptime max_levels: usize) !*Self {
        var node = try allocator.create(Element);
        node.forward = try allocator.alloc(?*Element, max_levels);
        @memset(node.forward, null);
        node.key = key;
        node.value = val;
        return node;
    }

    pub fn printList(self: *Self) void {
        std.debug.print("{d}:{s} -> ", .{ self.key, self.value });
        if (self.forward.len > 0 and self.forward[0] != null) {
            self.forward[0].?.printList();
        }
    }
};

const SkipList = struct {
    head: *Element,
    comptime max_levels: usize = 10,
    level: usize,
    allocator: *std.mem.Allocator,

    const Self = @This();

    pub fn init(comptime max_levels: usize, allocator: *const std.mem.Allocator) !Self {
        var head = try Element.init((1 << 32) - 1, "~HEAD~", allocator, max_levels);
        return Self{
            .head = head,
            .max_levels = max_levels,
            .level = 1,
            .allocator = @constCast(allocator),
        };
    }

    pub fn deinit(self: *Self) void {
        var curr: ?*Element = self.head;
        while (curr) |c| {
            curr = c.forward[0];
            self.allocator.free(c.forward);
            self.allocator.destroy(c);
        }
    }

    fn randomLevel(self: *Self) usize {
        var lvl: usize = 0;
        var rng = std.rand.DefaultPrng.init(123453);
        var random = rng.random();

        while (true) {
            var t = random.boolean();
            if (!t or lvl + 1 == self.max_levels) {
                return lvl;
            var i = self.level;
            while (i >= 0) : (i -= 1) {
                std.debug.print("leveL: {d}\n", .{i});
                std.debug.print("length: {any}\n", .{curr.?.forward.len});
                std.debug.print("Node: {any}\n", .{curr.?.forward[i]});
                std.debug.print("Test\n", .{});
                while (curr.?.forward[i]) |element| : (curr = element) {
                    std.debug.print("Infinite\n", .{});
                    if (element.key >= key) break;
                }
                update[i] = curr;
            }

            lvl += 1;
        }

        return lvl;
    }

    pub fn printList(self: *Self) void {
        self.head.printList();
        std.debug.print("\n-END-\n", .{});
    }

    pub fn insert(self: *Self, key: u32, val: []const u8) !void {
        const num: usize = self.max_levels + 1;
        var update: [num]?*Element = [_]?*Element{null} ** num;
        var curr: ?*Element = self.head;

        var l: usize = self.level + 1;
        while (l > 0) : (l -= 1) {
            while (curr.?.forward[l - 1]) |next| : (curr = next) {
                if (next.key >= key) break;
            }
            update[l - 1] = curr;
        }

        // This WILL result in runtime panic if curr is null
        // if (optional_number) |number| {
        //     print("got number: {}\n", .{number});
        // }
        curr = curr.?.forward[0];

        if (curr == null or curr.?.key != key) {
            const rlvl = self.randomLevel();

            if (rlvl > self.level) {
                var idx = self.level + 1;
                while (idx < rlvl + 1) : (idx += 1) {
                    update[idx] = self.head;
                }
                self.level = rlvl;
            }

            var node = try Element.init(key, val, self.allocator, self.max_levels);
            var c: usize = 0;
            while (c < rlvl + 1) : (c += 1) {
                if (update[c]) |el| {
                    node.forward[c] = if (el.forward.len > c) el.forward[c] else null;
                    if (el.forward.len > c) {
                        el.forward[c] = node;
                    }
                }
            }
        }
    }

    pub fn delete(self: *Self, key: u32) void {
        var update: [self.max_levels]?*Element = [_]?*Element{null} ** self.max_levels;
        var curr: ?*Element = self.head;

        var l: usize = self.level + 1;
        while (l > 0) : (l -= 1) {
            while (curr.?.forward[l - 1]) |next| : (curr = next) {
                if (next.key >= key) break;
            }

            update[l - 1] = curr;
        }

        curr = curr.?.forward[0];
        if (curr.?.key == key) {
            var i: usize = 0;
            while (i < self.level) : (i += 1) {
                if (update[i].?.forward[i] != curr) break;
                update[i].?.forward[i] = curr.?.forward[i];
            }
            self.allocator.free(curr.?.forward);
            self.allocator.destroy(curr.?);
            while (self.level > 1 and self.head.forward[self.level] == null) {
                self.level = self.level - 1;
            }
        }
    }
};

test "insert" {
    const allocator = std.heap.page_allocator;
    var list = try SkipList.init(10, &allocator);

    try list.insert(8, "nate");
    try list.insert(1111, "something");
    try list.insert(1111000000, "zero");
    try list.insert(100100, "one-hundred");
    try list.insert(5, "one");
    try list.insert(1, "five");

    list.printList();

    list.delete(100100);
    list.printList();
    list.delete(5);
    list.printList();
    list.delete(1);
    list.printList();
    list.delete(1111);
    list.printList();
    list.delete(8);
    list.printList();
    list.delete(1111000000);

    list.printList();
    list.deinit();
}

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    var list = try SkipList.init(2, &allocator);

    try list.insert(8, "nate");
    try list.insert(1111, "something");
    try list.insert(1111000000, "zero");
    try list.insert(100100, "one-hundred");
    try list.insert(5, "one");
    try list.insert(1, "five");

    list.printList();

    list.delete(100100);
    list.printList();
    list.delete(5);
    list.printList();
    list.delete(1);
    list.printList();
    list.delete(1111);
    list.printList();
    list.delete(8);
    list.printList();
    list.delete(1111000000);

    list.printList();
    list.deinit();
}

test "element init" {}
