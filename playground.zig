const std = @import("std");
const mem = std.mem;
const builtin = @import("builtin");
const libc = @cImport("libc.h");

fn getPageSize() void {
    const pageSize = mem.page_size;
    const cpuArch = builtin.cpu.arch;
    const tag = builtin.os.tag;
    std.debug.print("PageSize: {d}\n", .{pageSize});
    std.debug.print("Arch: {any}\n", .{cpuArch});
    std.debug.print("CpuTag: {any}\n", .{tag});

    libc.time();
    switch (cpuArch) {
        .aarch64 => switch (tag) {
            .macos => std.debug.print("Found arch 64\n", .{}),
        },
        else => std.debug.print("Else!", .{}),
    }
}

pub fn main() void {
    getPageSize();
    return;
    // const num_levels: usize = 10;
    // var levels: [num_levels]?i32 = [_]?i32{null} ** num_levels;

    // var i = num_levels - 1;
    // while (i > 0) : (i -= 1) {
    //     // const el = levels[i] orelse std.debug.print("not here: {?}\n", .{i});
    //     const el = levels[i] orelse continue;
    //     std.debug.print("El: {?}\n", .{el});
    // }
    // std.debug.print("done", .{});
}
