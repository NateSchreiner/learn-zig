const std = @import("std");
const c = @cImport({
    @cInclude("hello.c");
});

pub fn main() !void {
    const return_signal = c.helloWorld();
    std.debug.print("Found return signal: {d}\n", .{return_signal});
}
