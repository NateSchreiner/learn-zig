const std = @import("std");
const builtin = @import("builtin");
// const ArrayList = @import("std").ArrayList;

pub fn main() !void {
    _ = try ask_user();
}

fn ask_user() !void {
    const stdin = std.io.getStdIn().reader();
    const stdout = std.io.getStdOut().writer();

    var quit: bool = false;

    while (quit == false) {
        var buffer: [1024]u8 = undefined;

        try stdout.print("A number please: ", .{});

        if (try stdin.readUntilDelimiterOrEof(&buffer, '\n')) |user_input| {
            std.debug.print("UserInputLength: {}\n", .{user_input.len});
            std.debug.print("Line: {s}\n", .{user_input});
            if (std.mem.eql(u8, user_input, "quit")) {
                quit = true;
            }
            // return std.fmt.parseInt(i64, user_input, 10);
        }
    }
    // comment
    // these lines
    // if you would please
    // and thank you much
}
