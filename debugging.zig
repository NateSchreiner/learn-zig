const std = @import("std");

// Attach CodeLLDB to Zig tests
// https://gist.github.com/floooh/31143278a0c0bae4f38b8722a8a98463

// launch.json
// {
//     "version": "0.2.0",
//     "configurations": [
//         {
//             "name": "(lldb) Launch",
//             "type": "lldb",
//             "request": "launch",
//             "program": "${workspaceFolder}/zig-out/bin/app",
//             "cwd": "${workspaceFolder}",
//             "preLaunchTask": "build"
//         }
//     ]
// }

// tasks.json
//   {
//     "version": "2.0.0",
//     "tasks": [
//         {
//             "label": "build",
//             "type": "shell",
//             "command": "zig build",
//             "problemMatcher": [],
//             "group": {
//                 "kind": "build",
//                 "isDefault": true
//             }
//         }
//     ]
// }
