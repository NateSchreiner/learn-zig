const std = @import("std");
// RocksDB Skiplist Implementation
// https://github.com/facebook/rocksdb/blob/main/memtable/inlineskiplist.h

// SkipList holds an array of Elements
// `Element` is a list of Nodes. with left pointers
// When we add a value, were adding an Element which defaults to num_levels in length
// How many Nodes we add to this element would be calculated by the coin flip
// How would Nodes left pointers be set though?

const num_levels = 5; // Make configurable?

pub fn SkipList(comptime T: type) type {
    return struct {
        const Self = @This();

        head: ?*Element(T),
        level: usize,
        size: usize,
        allocator: std.mem.Allocator,

        pub fn init(alloc: std.mem.Allocator) !Self {
            const t = Element(T);
            var node = try t.init(alloc);
            return .{
                .head = &node,
                .level = 0,
                .size = 0,
                .allocator = alloc,
            };
        }

        pub fn Set(self: *Self, key: T, val: T) !void {
            var update = try self.allocator.alloc(?*Element(T), num_levels + 1);
            @memset(update, null);
            // var update = std.ArrayList(*Element(T)).init(self.allocator);
            var curr = self.head;

            var i = self.level;
            while (i >= 0) : (i -= 1) {
                std.debug.print("leveL: {d}\n", .{i});
                std.debug.print("length: {any}\n", .{curr.?.forward.len});
                std.debug.print("Node: {any}\n", .{curr.?.forward[i]});
                std.debug.print("Test\n", .{});
                while (curr.?.forward[i]) |element| : (curr = element) {
                    // std.debug.print("Infinite\n", .{});
                    // std.debug.print("Comparing: {any} to {any}\n", .{ element.key, key });
                    if (element.key >= key) break;
                }
                update[i] = curr;
            }

            curr = curr.?.forward[0];

            if (curr) |elem| {
                if (elem.key == key) {
                    std.debug.print("Found key\n", .{});
                    elem.value = val;
                } else {
                    std.debug.print("No find...\n", .{});
                    var lvl = randomLevel(num_levels);
                    if (lvl > self.level) {
                        var idx = self.level + 1;
                        while (idx < lvl) : (idx += 1) {
                            update[i] = self.head;
                        }
                        self.level = lvl;
                    }

                    // We need to actually make the new node here
                    const e = Element(T);
                    var new_node = try e.init();
                    // new_node.makeNode(lvl);
                    var idx = 0;
                    while (idx < self.level) : (idx += 1) {
                        new_node.forward[i] = update[i].foward[i];
                        update[i].forward[i] = new_node;
                    }
                }
            }
        }

        fn generateLevel(self: *Self, lvl: usize) void {
            var tmp = self.head;

            const t = Element(T);
            var node = try t.init();
            self.head = &node;

            self.level = lvl;
            std.mem.copy(Element(T), self.head.nodes, tmp.nodes);
        }

        fn randomLevel(max: usize) usize {
            var lvl = 1;
            while (true) {
                var t = std.rand.Random.boolean();
                if (!t or lvl == max) {
                    return lvl;
                }

                lvl += 1;
            }
        }
    };
}

pub fn Element(comptime T: type) type {
    return struct {
        const Self = @This();

        key: T,
        value: T,
        forward: [num_levels]?*Element(T),
        // @memset()
        pub fn init(alloc: std.mem.Allocator) !Self {
            const elType = ?*Element(T);
            var elems = try alloc.alloc(elType, num_levels);
            for (elems) |*item| {
                item.* = null;
            }

            return Self{ .forward = elems[0..5].*, .key = std.math.maxInt(i32), .value = std.math.maxInt(i32) };
        }

        // pub fn makeNode(self: *Self, lvl: usize) void {
        //     var i = 0;
        //     while (i < lvl) : (i += 1) {
        //         self.forward[i] =
        //     }
        // }

        fn getLevels() usize {
            return 0;
        }
    };

    // Adds a new element in sorted order.  Adjust's pointers at prev and next at level
}

test "init" {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const IntList = SkipList(i32);
    var list = try IntList.init(allocator);
    std.debug.print("Inserting...\n", .{});
    try list.Set(8, 10);

    std.debug.print("List Levels: {d}\n", .{list.level});
    std.debug.print("{any}\n", .{list.head.?.forward});
}
