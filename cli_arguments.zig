const std = @import("std");
// Run with `zig run cli_arguments.zig -- <argument> <list>

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    const args = try std.process.argsAlloc(allocator);
    std.debug.print("Args?: {s}\n", .{args});
}
